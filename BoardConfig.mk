# Kernel
TARGET_KERNEL_CONFIG := cm_j2lte_defconfig

# Recovery
TARGET_OTA_ASSERT_DEVICE := j2lte,j2ltexx,SM-J200G,SM-J200GU,SM-J200F,SM-J200M

# Inherit common board flags
include device/samsung/j2lte-common/BoardConfigCommon.mk